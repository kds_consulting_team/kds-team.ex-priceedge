import logging
import os
import sys
from kbc.client_base import HttpClientBase


class PriceEdgeClient(HttpClientBase):

    def __init__(self, apiUrl, username, password, clientId):

        self.parApiUrl = apiUrl
        self.parUsername = username
        self.parPassword = password
        self.parClientId = clientId

        super().__init__(self.parApiUrl)
        self.getToken()

    def getToken(self):

        urlToken = os.path.join(self.base_url, 'token')
        headerToken = {
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded'
        }
        bodyToken = {
            'grant_type': 'password',
            'username': self.parUsername,
            'password': self.parPassword,
            'client_id': self.parClientId
        }

        reqToken = self.post_raw(urlToken, headers=headerToken, data=bodyToken)
        scToken, jsToken = reqToken.status_code, reqToken.json()

        if scToken == 200:

            logging.info("Access token acquired.")
            self.varToken = jsToken['access_token']
            self._auth_header = {
                'Authorization': f'Bearer {self.varToken}',
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }

        else:

            logging.error(f"Could not get access token. Received: {scToken} - {jsToken}.")
            sys.exit(1)

    def getChangedPrices(self):

        urlPrices = os.path.join(self.base_url, 'api/items/ChangedPrices')
        headerPrices = {
            'Content-Type': 'application/json',
            'client_id': self.parClientId
        }
        _pageNumber = 0
        _itemsPerPage = 1000

        allItems = []
        payloadComplete = False

        while payloadComplete is False:
            _pageNumber += 1

            bodyPrices = {
                'PageNumber': _pageNumber,
                'RecordsOnPage': _itemsPerPage
            }

            reqPrices = self.post_raw(urlPrices, headers=headerPrices, json=bodyPrices)
            scPrices, jsPrices = reqPrices.status_code, reqPrices.json()

            if scPrices == 200:
                allItems += jsPrices['Result']

                if len(jsPrices['Result']) < _itemsPerPage:
                    payloadComplete = True

            else:
                logging.error("There was an error downloading changed prices.")
                logging.error(f"Received: {scPrices} - {jsPrices}.")
                sys.exit(1)

        return allItems
