import logging
from kbc.env_handler import KBCEnvHandler
from priceedge.client import PriceEdgeClient
from priceedge.result import PriceEdgeWriter

KEY_APIURL = 'api_url'
KEY_USERNAME = 'username'
KEY_PASSWORD = '#password'
KEY_CLIENTID = 'client_id'

MANDATORY_PARAMETERS = [KEY_APIURL, KEY_USERNAME, KEY_PASSWORD, KEY_CLIENTID]


class PriceEdgeComponent(KBCEnvHandler):

    def __init__(self):

        super().__init__(mandatory_params=MANDATORY_PARAMETERS)
        self.validate_config(mandatory_params=MANDATORY_PARAMETERS)

        self.parApiUrl = self.cfg_params[KEY_APIURL]
        self.parUsername = self.cfg_params[KEY_USERNAME]
        self.parPassword = self.cfg_params[KEY_PASSWORD]
        self.parClientId = self.cfg_params[KEY_CLIENTID]

        self.client = PriceEdgeClient(apiUrl=self.parApiUrl,
                                      username=self.parUsername,
                                      password=self.parPassword,
                                      clientId=self.parClientId)

        self.writer = PriceEdgeWriter(self.data_path, 'changed_prices')

    def run(self):

        logging.info("Obtaining changed prices.")
        allItems = self.client.getChangedPrices()
        self.writer.writerows(allItems)
