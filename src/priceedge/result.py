import csv
import json
import os

FIELDS_CHANGED_PRICES = ['RowNumber', 'ItemNumber', 'PriceListCode', 'PriceListName', 'PriceListTypeCode',
                         'PriceListTypeName', 'PriceListCurrency', 'UnitOfMeasure', 'StartDate', 'EndDate', 'Price']
PK_CHANGED_PRICES = ['ItemNumber', 'PriceListCode', 'StartDate', 'EndDate']


class PriceEdgeWriter:

    def __init__(self, dataPath, tableName):

        self.paramPath = dataPath
        self.paramTableName = tableName
        self.paramTable = tableName + '.csv'
        self.paramTablePath = os.path.join(self.paramPath, 'out/tables', self.paramTable)
        self.paramFields = eval(f'FIELDS_{tableName.upper()}')
        self.paramPrimaryKey = eval(f'PK_{tableName.upper()}')

        self.createManifest()
        self.createWriter()

    def createManifest(self):

        template = {
            'incremental': True,
            'primary_key': self.paramPrimaryKey
        }

        path = self.paramTablePath + '.manifest'

        with open(path, 'w') as manifest:
            json.dump(template, manifest)

    def createWriter(self):

        self.writer = csv.DictWriter(open(self.paramTablePath, 'w'), fieldnames=self.paramFields,
                                     restval='', extrasaction='ignore', quotechar='\"', quoting=csv.QUOTE_ALL)
        self.writer.writeheader()

    def writerows(self, listToWrite):

        for row in listToWrite:
            self.writer.writerow(row)
